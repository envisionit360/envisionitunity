﻿using UnityEngine;
using System.Collections;

public class HouseObject : MonoBehaviour
{

    public int id;
    public Category category;
    public Option currentOption;

    public void setOption(Option o)
    {
        currentOption = o;
        GetComponent<Renderer>().material.mainTexture = currentOption.texture;
    }
}
