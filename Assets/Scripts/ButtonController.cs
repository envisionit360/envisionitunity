﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ButtonController : MonoBehaviour
{

    public Option option;
    public Transform associatedObject;

    public DB_Interface dbinterface;

    public void initializeButton()
    {
        transform.GetChild(0).GetComponent<Text>().text = option.name;
        GetComponent<Button>().onClick.AddListener(() => { setObjectMaterial(); });
    }

    public void setObjectMaterial()
    {
        if (option.textureLoaded)
        {
            option.assignToHouseObject(associatedObject);
        }
        else
        {
            dbinterface.GetComponent<UIController>().log("Requesting " + option.file);

            dbinterface.getTexture(associatedObject, option);
        }

    }
}
