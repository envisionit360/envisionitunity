﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{

    public Camera cam;
    public House house;

    // Use this for initialization
    //void Start () {
    //       cam = Camera.main;

    //}

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonUp(0))
        {
            Ray r = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, 1000) && house.isReady)
            {
                GetComponent<UIController>().GenerateMenu(hit.transform);
            }
        }
    }
}
