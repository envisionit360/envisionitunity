﻿using UnityEngine;
using System.Collections;

public class DB_Interface : MonoBehaviour
{


    private string rootURL = "https://envision-it-360-chandler-childs.c9users.io";
    private string dataIndex = "/gameData";
    private string requestParam = "?request=";
    private string updateParam = "?update=";
    private string listParam = "&list=";

    //private string categoryParam = "&category=";
    //private string idParam = "&id=";
    //private string houseParam = "&house=";


    public void getHouseData(House h)
    {
        StartCoroutine(getHouseCoroutine(h));
    }
    IEnumerator getHouseCoroutine(House h)
    {
        string requestURL = rootURL + dataIndex + requestParam + "house";
        GetComponent<UIController>().log("requesting: " + requestURL);
        WWW www = new WWW(requestURL);
        GetComponent<UIController>().log("received: " + requestURL);
        yield return www;
        if (www.error != null)
            print("Error: " + www.error);
        else
            h.setData(www.text);

    }


    public void getAllOptions(House h)
    {
        StartCoroutine(getAllOptionsCoroutine(h));
    }
    IEnumerator getAllOptionsCoroutine(House h)
    {
        string requestURL = rootURL + dataIndex + requestParam + "options";
        WWW www = new WWW(requestURL);
        yield return www;
        if (www.error != null)
            print("Error: " + www.error);
        else
            h.setOptions(www.text);
    }


    public void getTexture(Transform houseObject, Option option)
    {
        StartCoroutine(getTextureCoroutine(houseObject, option));
    }
    IEnumerator getTextureCoroutine(Transform houseObject, Option option)
    {
        WWW www = new WWW(rootURL + option.file);
        yield return www;
        if (www.error != null)
        {
            print("Error: " + www.error);
        }
        else
        {
            option.setTexture(www.texture);
            option.assignToHouseObject(houseObject);
        }
    }

    public void updateOptionSelections(string optionIDs)
    {
        StartCoroutine(updateOptionSelectionsCoroutine(optionIDs));
    }
    IEnumerator updateOptionSelectionsCoroutine(string optionIDs)
    {
        WWW www = new WWW(rootURL + dataIndex + updateParam + "selectedOptions" + listParam + optionIDs);
        yield return www;
        if (www.error != null)
            print(www.error);
        else
            print("New Selections Saved");

    }

    public void getSelectedOptions(House h)
    {
        StartCoroutine(getSelectedOptionsCoroutine(h));
    }
    IEnumerator getSelectedOptionsCoroutine(House h)
    {
        string request = rootURL + dataIndex + requestParam + "options" + "&selectedOptions=true";
        WWW www = new WWW(request);
        yield return www;
        if (www.error != null)
            print(www.error);
        else
        {
            h.setSelectedOptions(www.text);
        }
    }

    public void get3DHouse(House h)
    {
        StartCoroutine(get3DHouseCoroutine(h));
    }
    IEnumerator get3DHouseCoroutine(House h)
    {
        string request = rootURL + h.file;
        print(request);
        WWW www = new WWW(request);
        yield return www;
        if (www.error != null)
            print(www.error);
        else
        {
            GetComponent<UIController>().log("finished fbx download");
            h.dealWith3Dhouse(www);
        }

    }

}

