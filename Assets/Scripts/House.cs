﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class HouseData
{
    public int id;
    public string name;
    public string file;
    public string meta;
    public string picture;
    public string request_text;
}

public class House : MonoBehaviour
{

    public UIController uicontroller;
    public DB_Interface dbinterface;
    public GameObject spawnPoint;

    private int id;
    private string name;
    public string file;
    private string meta;
    private string picture;
    public bool isReady = false;

    public List<Option> allOptions = new List<Option>();
    List<Category> categories = new List<Category>();
    List<HouseObject> houseObjects = new List<HouseObject>();


    void Start()
    {
        uicontroller.log("Starting Game");
        uicontroller.setHouseTitle("Loading...");
        uicontroller.log("Loading House");
        dbinterface.getHouseData(this);
    }


    public void setData(string response)
    {
        HouseData temp = JsonUtility.FromJson<HouseData>(response);
        id = temp.id;
        name = temp.name;
        file = temp.file;
        picture = temp.picture;
        meta = temp.meta;

        uicontroller.setHouseTitle(name);

        dbinterface.getAllOptions(this);
        uicontroller.log("Loading Options...");
    }


    public void setOptions(string respons)
    {
        uicontroller.log("parsing options");

        string jsonArray = respons.Substring(respons.IndexOf('[') + 1, respons.IndexOf(']') - respons.IndexOf('[') - 1);
        string[] jsonObjects = jsonArray.Split('}');

        foreach (string s in jsonObjects)
        {
            string obj = s.Trim().Trim(',') + '}';
            if (obj.Trim().Trim('}') != "")
            {
                //Option o = JsonUtility.FromJson<Option>(obj);
                Option o = new Option(obj);
                allOptions.Add(o);
                uicontroller.log("Adding: " + o.name);
            }
        }

        categorizeOptions();
        categorizeHouseObjects();
        //exampleCategorizeStuff();
        isReady = true;
    }

    void categorizeOptions()
    {
        HashSet<string> categoriesStrings = new HashSet<string>();
        foreach (Option o in allOptions)
        {
            categoriesStrings.Add(o.category);
        }

        foreach (string categorName in categoriesStrings)
        {
            Category C = new Category(categorName);
            foreach (Option o in allOptions)
            {
                if (o.category == categorName)
                {
                    C.options.Add(o);
                }
            }
            categories.Add(C);
        }
        uicontroller.log("Finished Categorizing Options");
    }

    void categorizeHouseObjects()
    {
        Transform[] children = gameObject.GetComponentsInChildren<Transform>();

        foreach (Transform child in children)
        {
            child.gameObject.AddComponent<HouseObject>();
            string categoryName = extrapolateCategoryName(child.gameObject);
            if (categoryName == "Unknown")
                continue;
            else
            {
                child.gameObject.GetComponent<HouseObject>().category = categories.Find(x => x.name == categoryName);
                houseObjects.Add(child.gameObject.GetComponent<HouseObject>());
            }
        }
        uicontroller.log("Finished Categorizing Objects");

        getSelectedOptions();
    }


    string extrapolateCategoryName(GameObject obj)
    {
        if (obj.name.ToLower().Contains("wall"))
            return "Walls";                             //obj.GetComponent<HouseObject>().category = "Walls";
        else if (obj.name.ToLower().Contains("counter"))
            return "Countertops";                       // obj.GetComponent<HouseObject>().category = "Countertops";
        else if (obj.name.ToLower().Contains("floor"))
            return "Flooring";                          // obj.GetComponent<HouseObject>().category = "Flooring";
        else
            return "Unknown";
    }


    public void setSelectedOptions(string respons)
    {
        List<Option> selectedOptions = new List<Option>();
        string jsonArray = respons.Substring(respons.IndexOf('[') + 1, respons.IndexOf(']') - respons.IndexOf('[') - 1);
        string[] jsonObjects = jsonArray.Split('}');

        foreach (string s in jsonObjects)
        {
            string obj = s.Trim().Trim(',') + '}';
            if (obj.Trim().Trim('}') != "")
            {
                //Option o = JsonUtility.FromJson<Option>(obj);
                Option o = new Option(obj);
                selectedOptions.Add(o);
            }
        }

        foreach (Option opt in selectedOptions)
        {
            HouseObject obj = houseObjects.Find(x => x.category.name == opt.category);
            Option o = obj.category.options.Find(x => x.id == opt.id);


            if (o.textureLoaded)
            {
                o.assignToHouseObject(obj.transform);
            }
            else
            {
                dbinterface.GetComponent<UIController>().log("Requesting " + o.file);
                dbinterface.getTexture(obj.transform, o);
            }
        }


    }

    void getSelectedOptions()
    {
        dbinterface.getSelectedOptions(this);
    }


    public void dealWith3Dhouse(WWW response)
    {
        AssetBundle ab = response.assetBundle;
        int x = 0;
    }
}