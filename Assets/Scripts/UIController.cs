﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIController : MonoBehaviour
{

    public House house;
    public DB_Interface dbinterface;

    public Text houseTitle;
    public Text eventLog;
    public Transform menuHeader;
    public Transform buttonPrefab;
    //public Button saveButton;
    //public Button loadHouseButton;

    public Category currentCategory;
    public List<Transform> currentButtons = new List<Transform>();

    public void setHouseTitle(string title)
    {
        houseTitle.text = title;
    }

    public void log(string text)
    {
        //print(text);
        eventLog.text += "\n" + text;
    }

    public void GenerateMenu(Transform clickedObject)
    {
        if (clickedObject.GetComponent<HouseObject>())
        {
            GetComponent<UIController>().log("Clicked " + clickedObject.GetComponent<HouseObject>().category.name + " object");

            foreach (Transform button in currentButtons)
            {
                Destroy(button.gameObject);
            }
            currentButtons.Clear();

            currentCategory = clickedObject.GetComponent<HouseObject>().category;

            menuHeader.GetComponent<Text>().text = currentCategory.name;

            Vector3 nextButtonPosition = menuHeader.localPosition;
            nextButtonPosition.y -= 35;

            foreach (Option option in currentCategory.options)
            {
                Transform button = (Transform)Instantiate(buttonPrefab, Vector2.zero, Quaternion.identity);
                button.SetParent(transform, false);
                button.localPosition = nextButtonPosition;
                nextButtonPosition.y -= 30;

                currentButtons.Add(button);
                ButtonController controller = button.GetComponent<ButtonController>();
                controller.option = option;
                //controller.buttonText = option.name;
                //controller.fileURL = option.file;
                controller.dbinterface = GetComponent<DB_Interface>();
                controller.associatedObject = clickedObject;
                controller.initializeButton();
            }
        }
    }

    public void saveSelectedOptions()
    {
        log("Saving Option Selection");
        Transform[] blocks = house.gameObject.GetComponentsInChildren<Transform>();

        List<HouseObject> objectsList = new List<HouseObject>();
        foreach (Transform block in blocks)
        {
            HouseObject obj = block.gameObject.GetComponent<HouseObject>();
            objectsList.Add(obj);
        }

        string selections = "";
        foreach (HouseObject obj in objectsList)
        {
            if (obj.currentOption != null)
            {
                selections += obj.currentOption.id.ToString() + ',';
            }
        }
        selections.Trim(',');

        if (selections != "")
        {
            log("sending update request");
            GetComponent<DB_Interface>().updateOptionSelections(selections);
        }

    }

    public void load3Dhouse()
    {
        dbinterface.get3DHouse(house);
    }

}
