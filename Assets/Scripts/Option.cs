﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class OptionData
{
    public int id;
    public string name;
    public string file;
    public string category;
}


public class Option { 
    public int id;
    public string name;
    public string file;
    public string category;

    public Texture texture;
    public bool textureLoaded = false;

    //public Option() { }
    //public Option(OptionData data)
    //{
    //    id = data.id;
    //    name = data.name;
    //    file = data.file;
    //    category = data.category;
    //}
    public Option(string json)
    {
        OptionData obj = JsonUtility.FromJson<OptionData>(json);
        id = obj.id;
        name = obj.name;
        file = obj.file;
        category = obj.category;
    }


    public void setTexture(Texture t)
    {
        texture = t;
        textureLoaded = true;
    }
    
    public void assignToHouseObject(Transform houseObject)
    {
        houseObject.GetComponent<HouseObject>().setOption(this);
    }

    public void assignToHouseObject(HouseObject houseObject)
    {
        houseObject.setOption(this);

    }

}



public class Category
{
    public string name;
    public List<Option> options = new List<Option>();

    public Category(string n)
    {
        name = n;
    }
}